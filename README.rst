Steps:
1. Create a virtual environment with python>=3.6

    $ python -m venv .venv


2. Install requirements

    $ python -m pip install -r requirements/local.txt

    $ python -m pip install -r requirements/scrapper.txt


3. Create a postgres database for project: review_signals


4. Activate environment
    $ source .venv/bin/activate


5. Make migrations
    $ python manage.py migrate



6. To process row data stored in mongo db we run following management
   command:

    $ python manage.py minedata

    This command process the data in mongodb and we apply our 
    algorithm to rank the product and give modified average rating

7. Scrapdata management command
    Note: Use either -k or -kf flag. Not both together
    Example:

    -k

    $ python3 manage.py scrapdata -c amazon_datax -d amazon_data -k "Best gaming mouse"

    -kf

    $ python3 manage.py scrapdata -c amazon_datax -d amazon_data -kf ./keywords_test.txt

8. To run the website use runserver management command
 
    $ python manage.py runserver


