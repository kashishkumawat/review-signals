from django import forms
from django.urls import reverse


class KeywordSearchForm(forms.Form):
    keyword_search = forms.CharField(max_length=220)
    keyword_search_slug = forms.CharField(max_length=220)
    
    class Meta:
        fields = ("keyword_search", "keyword_search_slug")

    def __init__(self, *args, **kwargs):
        super(KeywordSearchForm, self).__init__(*args, **kwargs)
        self.fields['keyword_search_slug'].widget = forms.HiddenInput()
        self.fields['keyword_search'].widget.attrs['class'] = "form-control form-control-lg mr-sm-2 home-form-input"
        self.fields['keyword_search'].widget.attrs['type'] = "search"
        self.fields['keyword_search'].widget.attrs['id'] = "keyword-autocomplete"
        self.fields['keyword_search'].widget.attrs['placeholder'] = "Gaming Mouse..."
        self.fields['keyword_search'].widget.attrs['aria-label'] = "Search"
        self.fields['keyword_search'].widget.attrs['url'] = reverse('core:ajax_get_keywords')
        