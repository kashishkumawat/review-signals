from django.conf import settings


def export_variables(request):
    context = {}
    context['GOOGLE_VERIFICATION_CODE'] = ''
    context['BING_VERIFICATION_CODE'] = ''
    context['BASE_DOMAIN'] = 'https://reviewsignals.com'
    context['SITE_NAME'] = 'Review Signals'
    context['LOCALE'] = 'en_US'
    context['TWITTER_USERNAME'] = ''
    return context
