from django.core.management.base import BaseCommand
from django.utils import timezone

from review_signals.utils.analysis.review_analysis import Report

class Command(BaseCommand):
    help = 'Insert scrapped data from Mongo to Postgres and generate report for products'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--collection', type=str, help='Change the mongo collection name')
        parser.add_argument('-d', '--database', type=str, help='Change the mongo database name')

    def handle(self, *args, **kwargs):
        collection_name = kwargs['collection']
        db_name = kwargs['database']

        if collection_name and db_name:
            try:
                Report(db_name = db_name, collection_name = collection_name).execute()
            except:
                import traceback; traceback.print_exc()
        else:
            self.stdout.write(
                self.style.WARNING(
                    'Mining data with default db name as: "review_signal", '
                    'and collection name as: "amazonData"'
                )
            )
            res = input('Do you want to continue? (Y/N): ')
            if res == "y" or res == "Y":
                try:
                    Report(db_name="review_signal", collection_name="amazonData").execute()
                except:
                    import traceback; traceback.print_exc()
            else:
                self.stdout.write(self.style.ERROR(f'Try again later'))




