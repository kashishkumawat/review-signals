from django.core.management.base import BaseCommand
from django.utils import timezone

from review_signals.core.spiders.amazon_spider import AmazonSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import os.path


class Command(BaseCommand):
    help = """
        Scrap the data(regularly) with the help of scrapy and push the data
        to the mongodb
        """

    def add_arguments(self, parser):
        parser.add_argument('-c', '--collection', type=str, help='Change the mongo collection name')
        parser.add_argument('-d', '--database', type=str, help='Change the mongo database name')
        parser.add_argument('-k', '--keyword', type=str, help='Add Keyword to scrap')
        parser.add_argument('-kf', '--keyword_file', type=str, help='Add Keyword file to scrap')


    def handle(self, *args, **kwargs):
        collection_name = kwargs['collection']
        db_name = kwargs['database']
        keyword = kwargs['keyword']
        keyword_file = kwargs['keyword_file']

        if keyword and keyword_file:
            self.stdout.write(self.style.ERROR("Don't provide both keyword and keyword file at a time."))
            return False

        if keyword_file and not os.path.isfile(keyword_file):
            self.stdout.write(self.style.ERROR("File path is invalid. Try again later!"))
            return False    

        try:
            process = CrawlerProcess(get_project_settings())
            if collection_name:
                process.settings.set('MONGO_COLLECTION', collection_name)
            if db_name:
                process.settings.set('MONGO_DATABASE', db_name)

            if keyword:
                process.crawl(AmazonSpider, keyword_file = None, keyword= keyword)
            elif keyword_file:
                process.crawl(AmazonSpider, keyword_file = keyword_file, keyword = None)

            process.start()

            self.stdout.write(self.style.SUCCESS('Scrapping Done'))
        except:
            import traceback; traceback.print_exc()
            self.stdout.write(self.style.ERROR('Scrapping failed!!'))
