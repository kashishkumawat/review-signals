from django.db import models
from django.contrib.postgres.fields import JSONField

from .base import BaseModel


class ProductReport(models.Model):
    product = models.OneToOneField('Product', on_delete=models.CASCADE, related_name='report')
    average_readability_score = models.FloatField(blank=True, default=0)
    average_ari = models.FloatField(blank=True, default=0)
    average_noun_count = models.FloatField(default=0)
    average_verb_count = models.FloatField(default=0)
    average_sentiment_score = models.FloatField(default=0)
    average_rating = models.FloatField()
    fake_reviews_count = models.IntegerField()
    rs_average_rating = models.FloatField()
    rs_average_sentiment_score = models.FloatField()
    rating_counter = JSONField()
    fake_reviews_rating_counter = JSONField()
    readability_level_count = JSONField()
    ari_score_count = JSONField()
    total_reviews = models.IntegerField(default=0)
    average_review_length = models.IntegerField(default=0)
    extra_data = JSONField()

    def __str__(self):
        return f'Report {self.product.name[:30]}'

    def get_absolute_url(self):
        return reverse('core:report', kwargs={'slug':self.slug})
