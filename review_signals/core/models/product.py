from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify

from .base import BaseModel, SeoBaseModel

class Product(SeoBaseModel, BaseModel):
    name = models.CharField(max_length=512)
    url = models.TextField()
    asin = models.CharField(max_length=512)
    rank = models.IntegerField(blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    category = models.ForeignKey('ProductCategory',
        related_name='products',
        on_delete=models.SET_NULL,
        null=True
    )
    price = models.CharField(max_length=10, null=True, blank=True)
    slug = models.SlugField(max_length=512)
    image = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    def get_report_url(self):
        return reverse('core:report', kwargs={'slug':self.slug})

class Category(BaseModel, SeoBaseModel):
    name = models.CharField(max_length=512)
    parent = models.ForeignKey('self',
        related_name='children',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    description = models.TextField()
    image = models.ImageField(null=True, blank=True)
    slug = models.SlugField(max_length=512)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('core:category', kwargs={'slug':self.slug})

class ProductCategory(BaseModel, SeoBaseModel):
    name = models.CharField(max_length=512)
    title = models.TextField()
    parent = models.ForeignKey('Category',
        related_name='product_categories',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    slug = models.SlugField(max_length=512, unique=True)
    image = models.ImageField(null=True, blank=True)
    class Meta:
        verbose_name_plural = 'Product categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('core:product_list', kwargs={'slug':self.slug})


@receiver(pre_save, sender=Product)
def my_callback(sender, instance, *args, **kwargs):
    instance.slug = slugify(instance.name)
