from django.db import models

from .product import Product


class Review(models.Model):
    review_id = models.CharField(max_length=30)
    stars = models.IntegerField()
    title = models.CharField(max_length=255)
    author_name = models.CharField(max_length=255)
    badges = models.CharField(max_length=255)
    comments_count = models.IntegerField()
    review_text = models.TextField()
    review_helpful_votes = models.IntegerField()
    date = models.DateField()
    image_count = models.IntegerField(default=0)

    def __str__(self):
        return self.review_id
