from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'review_signals.core'
    verbose_name = 'core'
