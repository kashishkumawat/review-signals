from django.contrib import admin
from . import models


class ProductReportAdmin(admin.ModelAdmin):
    search_fields = ('product__name', )

admin.site.register(models.Category)
admin.site.register(models.ProductCategory)
admin.site.register(models.Product)
admin.site.register(models.ProductReport, ProductReportAdmin)
