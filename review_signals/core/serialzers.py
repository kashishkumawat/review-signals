from . import models
from rest_framework import serializers

class KeywordApiSerialzer(serializers.ModelSerializer):
    value = serializers.CharField(source='name')
    class Meta:
        model = models.ProductCategory
        fields = [
            "id",
            "value",
            "slug"
        ]