from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView, View
from django.http import Http404, HttpResponse, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.db.models import Avg

from . import models
from rest_framework.generics import ListAPIView
from .serialzers import KeywordApiSerialzer
from .forms import KeywordSearchForm


class Landing(View):
    template_name = 'pages/home.html'
    form_class = KeywordSearchForm

    def get_context_data(self, **kwargs):
        # context = super().get_context_data(**kwargs)
        context = {}
        context['categories'] = models.Category.objects.all()[:4]
        context['product_categories'] = models.ProductCategory.objects.all().annotate(x=Avg('products__report__total_reviews')).order_by('-x')
        context["keyword_form"] = self.form_class
        return context

    def get(self, *args, **kwargs):
        return render(self.request, self.template_name, self.get_context_data())

    def post(self, *args, **kwargs):
        form = self.form_class(self.request.POST)
        keyword_slug = self.request.POST.get("keyword_search_slug")
        reverse_url = reverse("core:product_list", kwargs={"slug": keyword_slug})
        return HttpResponseRedirect(reverse_url)


class ProductCategoryListView(ListView):

    context_object_name = 'product_categories'
    paginate_by = 2

    def get_queryset(self):
        return models.ProductCategory.objects.filter(
            parent__name__icontains=self.kwargs.get('slug')
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = get_object_or_404(
            models.Category, slug=self.kwargs.get('slug')
        )
        return context


class ProductListView(ListView):
    context_object_name = "products"

    @property
    def get_product_category(self):
        return get_object_or_404(models.ProductCategory, slug=self.kwargs.get('slug'))

    def get_queryset(self):
        return self.get_product_category.products.filter(score__gt = 0).order_by('rank')[0:20]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['product_category'] = self.get_product_category
        return context

class ProductReportView(View):
    template_name = 'core/productreport_detail.html'

    def get(self, *args, **kwargs):
        product = models.Product.objects.filter(slug = kwargs['slug'])
        if len(product) == 0:
            return Http404
        else:
            product = product[0]

        product_report = product.report
        context = dict()
        context['report'] = product_report
        return render(self.request, self.template_name, context)


class getKeywordList(ListAPIView):
    serializer_class = KeywordApiSerialzer
    def get_queryset(self, *args, **kwargs):
        queryset = models.ProductCategory.objects.all()
        return queryset
