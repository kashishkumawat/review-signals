# Generated by Django 2.0.9 on 2019-03-30 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_auto_20190330_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='score',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
