# Generated by Django 2.0.9 on 2019-01-26 10:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_seometa'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='seo_meta',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.SeoMeta'),
        ),
        migrations.AddField(
            model_name='product',
            name='seo_meta',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.SeoMeta'),
        ),
        migrations.AddField(
            model_name='productcategory',
            name='seo_meta',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.SeoMeta'),
        ),
    ]
