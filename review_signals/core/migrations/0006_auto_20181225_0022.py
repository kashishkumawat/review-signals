# Generated by Django 2.0.9 on 2018-12-24 18:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20181224_2343'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productcategory',
            old_name='description',
            new_name='title',
        ),
        migrations.AlterUniqueTogether(
            name='productcategory',
            unique_together={('slug', 'parent')},
        ),
    ]
