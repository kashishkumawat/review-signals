# Generated by Django 2.0.9 on 2019-03-11 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20190126_1850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='rank',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='score',
            field=models.FloatField(blank=True),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='title',
            field=models.TextField(blank=True),
        ),
    ]
