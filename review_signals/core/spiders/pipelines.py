# -*- coding: utf-8 -*-

import pymongo
import json
from .items import (
    AmazonProductItem,
    AmazonReviewItem,
    AmazonKeywordItem
)
import tqdm


class AmazonScrapperPipeline(object):

    def __init__(self, mongo_uri, mongo_db, mongo_collection):
        self.collection_name = mongo_collection
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items'),
            mongo_collection=crawler.settings.get('MONGO_COLLECTION', 'items')
        )


    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.collection = self.db[self.collection_name]


    def close_spider(self, spider):
        c = self.collection_name
        print(f"Scrappy is closed. Checkout the {self.mongo_db} database, collection: {c} on mongo!!")
        print(f"Cleaning up the waste products from collection: {c}")
        self.cleanUp()
        self.client.close()


    def create_indexes(self):
        self.collection.create_index(
            {"keyword": 1, "products.asin": 1, "products.reviews.id": 1},
            {"unique": True }
        )

    def process_item(self, item, spider):
        if isinstance(item, AmazonKeywordItem):
            self.processKeyword(item)

        if isinstance(item, AmazonProductItem):
            self.processProduct(item)

        if isinstance(item, AmazonReviewItem):
            self.processReview(item)

        return item


    def processProduct(self, item):
        item = dict(item)
        keyword = item["keyword"]
        del item["keyword"]
        item.update({
            "reviews": []
        })

        self.collection.update(
            {"keyword": keyword,},
            {
                "$addToSet": {
                    "products": item
                }
            }
        )


    def processKeyword(self, item):
        item = dict(item)
        item.update({
            "products": []
        })

        self.collection.update(
            {"keyword": item["keyword"]},
            item,
            True
        )


    def processReview(self, item):
        item = dict(item)
        asin = item["asin"]
        del item["asin"]
        keyword = item["keyword"]
        del item["keyword"]

        self.collection.update(
            {"keyword": keyword, "products.asin": asin},
            {
                "$addToSet":{
                    "products.$.reviews": item
                }
            }
        )

    def cleanUp(self):
        # client = pymongo.MongoClient("mongodb://localhost:27017/")
        # db = client["amazon_data"]
        # collection = db["amazon_datax"]
        for keyword in self.collection.find():
            if len(keyword["products"]) == 0:
                self.collection.delete_one(keyword)
                continue

            for product in tqdm(keyword["products"], ascii = True, desc = "Product Deletion"):
                if len(product["reviews"]) == 0:
                    self.collection.update_one(
                        {
                            'keyword': keyword["keyword"],
                            'products.asin': product["asin"]
                        },
                        {
                            '$pull': {
                                "products": {
                                    'asin': product["asin"],
                                    'name': product["name"]
                                }
                            }
                        }
                    )