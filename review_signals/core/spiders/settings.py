# -*- coding: utf-8 -*-

# Scrapy settings for amazon_scrapper project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import os
BASE_DIR = os.path.dirname(os.path.realpath(__file__))
# BOT_NAME = 'amazon_scrapper'

# SPIDER_MODULES = ['amazon_scrapper.spiders']
# NEWSPIDER_MODULE = 'amazon_scrapper.spiders'


# LOG_LEVEL='INFO'
# LOG_ENABLED = False
# LOG_FILE =  "amazon_scrapper.log"


# MONGO_URI = "mongodb://localhost:27017/"
# MONGO_DATABASE = "amazon_data"


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'amazon_scrapper (+http://www.yourdomain.com)'

# Obey robots.txt rules
# ROBOTSTXT_OBEY = True
ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 100
DOWNLOAD_TIMEOUT = 10
DOWNLOAD_DELAY = 4

LOG_LEVEL='WARNING'
# LOG_ENABLED = False
# LOG_FILE =  "amazon_scrapper.log"


MONGO_URI = "mongodb://localhost:27017/"
MONGO_COLLECTION = "amazon_data"
MONGO_DATABASE = "amazon_data"

# to avoid getting loss data response set to false
DOWNLOAD_FAIL_ON_DATALOSS = False


DOWNLOADER_MIDDLEWARES = {
    'review_signals.core.spiders.middlewares.RotateUserAgentMiddleware': 110,
}

ITEM_PIPELINES = {
   'review_signals.core.spiders.pipelines.AmazonScrapperPipeline': 100,
}

SPIDER_MIDDLEWARES = {
   'review_signals.core.spiders.middlewares.StartRequestsCountMiddleware': 200,
}

EXTENSIONS = {
    'review_signals.core.spiders.logging.LogStats': 200,
    'review_signals.core.spiders.logging.MonitorDownloadsExtension': 100,
    'review_signals.core.spiders.logging.DumpStatsExtension': 101,
}

user_agent_file = os.path.join(BASE_DIR, "user_agents.txt")
USER_AGENTS = open(user_agent_file, "r").read().split("\n")

proxy_file = os.path.join(BASE_DIR, "proxies.txt")
PROXIES_LIST = open(proxy_file, "r").read().split("\n")


# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html


# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'amazon_scrapper.middlewares.AmazonScrapperDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'amazon_scrapper.pipelines.AmazonScrapperPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
