# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AmazonKeywordItem(scrapy.Item):
    # define the fields for your item here like:
    keyword = scrapy.Field()
    url = scrapy.Field()


class AmazonProductItem(scrapy.Item):
    # define the fields for your item here like:
    keyword = scrapy.Field()
    name = scrapy.Field()
    rank = scrapy.Field()
    price = scrapy.Field()
    total_reviews = scrapy.Field()
    asin = scrapy.Field()
    stars = scrapy.Field()
    img = scrapy.Field()  
    url = scrapy.Field()
    reviews = scrapy.Field()


class AmazonReviewItem(scrapy.Item):
    id = scrapy.Field()
    keyword = scrapy.Field()
    stars = scrapy.Field()
    title = scrapy.Field()
    author_name = scrapy.Field()
    badges = scrapy.Field()
    review_text= scrapy.Field()
    comments_count = scrapy.Field()
    review_helpful_votes = scrapy.Field()
    date = scrapy.Field()
    asin = scrapy.Field()

    image_count = scrapy.Field()