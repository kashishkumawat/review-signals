import scrapy
from scrapy.utils.project import get_project_settings
import urllib.parse
import re
from slugify import slugify
from .items import (
    AmazonProductItem,
    AmazonReviewItem,
    AmazonKeywordItem
)
import json
import random
from tqdm import tqdm


class AmazonSpider(scrapy.Spider):
    name = "amazon_spider"
    rotate_user_agent = True

    def __init__(self, *args, **kwargs):
        settings=get_project_settings()

        self.keyword = kwargs['keyword']
        self.keyword_file = kwargs['keyword_file']

        self.BASE_URL = "https://www.amazon.in"
        super(AmazonSpider, self).__init__(*args, **kwargs)
        # self.proxy_pool = settings.get('PROXIES_LIST')
        self.proxy_pool = []


    def start_requests(self):
        if self.keyword_file:
            keywordsFile = open(self.keyword_file, "r")
            keywords = keywordsFile.readlines()
        elif self.keyword:
            keywords = []
            keywords.append(self.keyword)

        for keyword in tqdm(keywords, ascii = True, desc = "Keyword"): 
            keyword_sluggified = slugify(keyword.strip())
            keyword_urlified = re.sub(r'-', '+', keyword_sluggified)
            keyword_url = self.getKeywordURL(keyword_urlified)
            fetch_products_request = scrapy.Request(
                url=keyword_url,
                meta={
                    "keyword_url": keyword_url,
                    "keyword_sluggified": keyword_sluggified,
                },
                callback=self.parse
            )
            if self.proxy_pool:
                fetch_products_request.meta['proxy'] = random.choice(self.proxy_pool)
            yield fetch_products_request


    def parse(self, response):
        meta = {
            "keyword": response.meta["keyword_sluggified"],
            "url": response.meta["keyword_url"],
        }
        response.meta.update(meta)
        yield AmazonKeywordItem(meta)

        yield from self.extractProducts(response)
        # yield from self.extractProductPages(response)


    def extractProductPages(self, response):
        try:
            next_page_xpath = ".//ul[@class='a-pagination']//li[@class='a-last']//a//@href"
            next_page = response.xpath(next_page_xpath).extract_first()
            
            if next_page:
                next_page_url = self.BASE_URL + next_page
                request = scrapy.Request(
                    url=next_page_url,
                    callback=self.extractProducts,
                    meta = response.meta 
                )
                if self.proxy_pool:
                    request.meta['proxy'] = random.choice(self.proxy_pool)
                yield request
        except Exception as e:
            return Exception("No next page available. All Product pages extracted")


    def extractProducts(self, response):
        products = response.css("div.s-result-item")[0:20]
        keyword = response.meta["keyword"]
        for product in tqdm(products, ascii = True, desc = f"Products: {keyword}", ncols = 80):
            asin = product.css('div::attr(data-asin)').extract_first()
            product = {
                'name': product.css('a.a-link-normal span.a-size-medium::text').extract_first() or
                        product.css('a.a-link-normal span.a-size-base-plus::text').extract_first(),
                'url':  product.css('a::attr(href)').extract_first(),
                'rank': product.css('div::attr(data-index)').extract_first(),
                'asin': asin,
                'price': product.css('span.a-price-whole::text').extract_first(),
                'total_reviews': product.css('a.a-link-normal span.a-size-base::text').extract_first(),
                'stars': self.extract_product_stars(product, asin),
                'img': product.css('img.s-image::attr(src)').extract_first(),
                "keyword": keyword
            }
            if product["name"] is not None:
                product_item = AmazonProductItem(product)
                yield product_item
                fetch_reviews_req = scrapy.Request(
                    url=self.getReviewPageURL(product["asin"]),
                    callback=self.parseReviews,
                    meta = {
                        "keyword": response.meta["keyword"],
                        "asin": product["asin"]
                    }
                )
                if self.proxy_pool:
                    fetch_reviews_req.meta['proxy'] = random.choice(self.proxy_pool)
                yield fetch_reviews_req



    def extract_product_stars(self, product, asin):
        star_text = product.css('i.a-icon-star-small span::text').extract_first()
        if star_text:
            return int(star_text.split()[0][0])
        else:
            return None


    def errback(self, err):
        """Handles an error"""
        return {
            'url': err.request.url,
            'status': 'error_downloading_http_response',
            'message': str(err.value),
        }

    def getKeywordURL(self, keyword):
        starturl = f"{self.BASE_URL}/s?k={keyword}&ref=nb_sb_noss_2"
        return starturl



    # """
    # Reviews Spider from here

    # """
    def parseReviews(self, response):
        yield from self.extractPageReviews(response)
        yield from self.extractReviewPages(response)


    def extractPageReviews(self, response):
        reviews_htm = response.css('div[data-hook=review]')
        asin = response.meta["asin"]
        keyword = response.meta["keyword"]
        for review_htm in tqdm(reviews_htm, ascii = True, desc = f"Reviews: {asin}", ncols = 70):
            review = {
                'id': review_htm.xpath('@id').extract_first(),
                'stars': self.extract_review_stars(review_htm),
                'title': review_htm.css('a.review-title span::text').extract_first(),
                'author_name': review_htm.xpath('.//span[@class="a-profile-name"]//text()').extract_first(),
                'badges': review_htm.xpath('.//span[@data-hook="avp-badge"]//text()').extract_first(),
                'review_text': '\n'.join(review_htm.css('span.review-text span::text').extract()),
                'comments_count': review_htm.css('span.review-comment-total::text').extract_first(),
                'review_helpful_votes': self.extract_review_votes(review_htm),
                "date":  review_htm.xpath('.//span[@data-hook="review-date"]//text()').extract_first(),
                "image_count" : len(review_htm.xpath(".//img[@alt='review image']")),
                "asin": asin,
                "keyword": keyword,
            }
            review_item = AmazonReviewItem(review)
            yield review_item


    def extractReviewPages(self, response):
        try:
            next_page = response.xpath(".//ul[@class='a-pagination']//li[@class='a-last']//a//@href").extract()
            next_page_url = self.BASE_URL + next_page[0]
            request = scrapy.Request(
                url=next_page_url,
                callback=self.parseReviews,
                meta = response.meta 
            )
            if self.proxy_pool:
                request.meta['proxy'] = random.choice(self.proxy_pool)
            yield request
        except:
            return Exception("No next page available. All Reviews pages extracted")


    def getReviewPageURL(self, asin, pageNumber = 1):
        starturl = "{}/product-reviews/{}/\
        ref=cm_cr_getr_d_show_all?ie=UTF8&reviewerType=\
        all_reviews&pageSize=20&pageNumber={}".format(self.BASE_URL, asin, pageNumber)
        url = starturl.split('\t')
        starturl = "".join(url)
        return starturl


    def extract_review_votes(self, review):
        votes = review.xpath('//span[@data-hook="helpful-vote-statement"]//text()').extract_first()
        if not votes:
            return 0
        votes = votes.strip().split(' ')
        if not votes:
            return 0
        votes = votes[0].replace(',', '')
        if votes == "One":
            votes = 1
        return votes

    def extract_review_stars(self, review):
        stars = None
        star_classes = review.css('i.a-icon-star::attr(class)').extract_first().split(' ')
        for i in star_classes:
            if i.startswith('a-star-'):
                stars = int(i[7:])
                break
        return stars