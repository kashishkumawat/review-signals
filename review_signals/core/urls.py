from django.urls import path
from .views import  (
    Landing, 
    ProductCategoryListView, 
    ProductListView, 
    ProductReportView,
    getKeywordList
)

urlpatterns = [
    path('', Landing.as_view(), name='landing'),
    path('category/<slug:slug>', ProductCategoryListView.as_view(), name='category'),
    path('<slug:slug>', ProductListView.as_view(), name='product_list' ),
    path('<slug:slug>/report', ProductReportView.as_view(), name='report'),
    path('ajax/get/keyword/list', getKeywordList.as_view(), name='ajax_get_keywords'),
]
