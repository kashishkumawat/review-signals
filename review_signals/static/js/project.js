/* Project specific Javascript goes here. */
// $("input").attr("autocomplete", "off");
function KeywordAutoComplete(data, idName = "#keyword-autocomplete"){
    $(idName).autocomplete({
      lookup: data,
      preventBadQueries: true,
      lookupLimit: 5,
      minChars: 0,
      autoSelectFirst : false,
      showNoSuggestionNotice:  true,

      onSelect: function(suggestion) {
            $("#id_keyword_search_slug").attr("value", suggestion.slug);
      },

      lookupFilter: function (suggestion, query, queryLowerCase) {
            return suggestion.value.toLowerCase().indexOf(queryLowerCase) == 0;
        },
  });
}

function getKeywords(){
    $.ajax({
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        url :  $("#keyword-autocomplete").attr("url"),
        datatype : 'json',
        success : function(response){
            KeywordAutoComplete(response)
        },
        error: function(response){
            alert("Error in fetching keywords!!")
        }
    });

}

$(document).ready(function(){
    getKeywords();

})

