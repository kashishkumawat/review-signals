
import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
import pickle
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from nltk.classify import ClassifierI
from statistics import mode
from nltk.tokenize import word_tokenize
import re

class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)

    def confidence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)
        return conf


class SentimentAnalysis:
    def __init__(self):
        self.loadDocumentPickle()
        self.loadWordFeaturesPickle()
        self.loadClassifierModelPickle()

        random.shuffle(self.featuresets)
        self.testing_set = self.featuresets[10000:]
        self.training_set = self.featuresets[:10000]


    def loadDocumentPickle(self):
        documents_f = open("pickled_algos/documents.pickle", "rb")
        self.documents = pickle.load(documents_f)
        documents_f.close()

    def loadWordFeaturesPickle(self):
        word_features5k_f = open("pickled_algos/word_features5k.pickle", "rb")
        self.word_features = pickle.load(word_features5k_f)
        word_features5k_f.close()
    

    def loadClassifierModelPickle(self):
        open_file = open("pickled_algos/originalnaivebayes5k.pickle", "rb")
        self.naive_bayes_classifier = pickle.load(open_file)
        open_file.close()

        open_file = open("pickled_algos/MNB_classifier5k.pickle", "rb")
        self.MNB_classifier = pickle.load(open_file)
        open_file.close()

        open_file = open("pickled_algos/BernoulliNB_classifier5k.pickle", "rb")
        self.BernoulliNB_classifier = pickle.load(open_file)
        open_file.close()
 

    def find_features(self, document):
        words = word_tokenize(document)
        features = {}
        for w in self.word_features:
            features[w] = (w in words)
        return features


    def classifier(self, document):
        voted_classifier = VoteClassifier(self.naive_bayes_classifier,
                                          self.MNB_classifier,
                                          self.BernoulliNB_classifier,
                                         )

        features = self.find_features(document)
        return (
            voted_classifier.classify(features),
            voted_classifier.confidence(features)
        )

