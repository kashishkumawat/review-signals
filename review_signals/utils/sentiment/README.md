# Sentiment analysis of product reviews

## Model 1: Naive Bayes Classification algorithm

Efficiency: 80.2%

### How to test the model?

Import the class, create the object, call the method classify with the review.


```
from sentiment import SentimentAnalysis

s = SentimentAnalysis()
s.classifier("This product is not good. Do not buy this!!")

```
