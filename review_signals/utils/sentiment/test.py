from sentiment import SentimentAnalysis
import re

s = SentimentAnalysis()

test_file = open("./test.ft.txt", "r", encoding="utf-8")

failed = 0
totalReviews = 5000

test_set = [test_file.readline() for _ in range(totalReviews)]

for i,line in enumerate(test_set):
    doc = re.split(r"__label__(\d+) ", str(line))[1:]
    review_sent_actual = "nag" if doc[0] == "1" else "pos"
    review = doc[1]
    if not review_sent_actual == s.classifier(review)[0]:
        failed += 1

print("Total failed: ", failed)
print("Total passed: ", totalReviews-failed)
print("accuracy = ", (1-failed/totalReviews)*100, "%")

