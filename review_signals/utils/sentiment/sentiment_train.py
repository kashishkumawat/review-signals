#!/usr/bin/env python
# coding: utf-8

# In[97]:

## processor used: 
## https://ark.intel.com/content/www/us/en/ark/products/97479/intel-xeon-processor-e3-1270-v6-8m-cache-3-80-ghz.html
import pandas as pd
import numpy as np
import re
import os
from nltk.corpus import stopwords
import pickle
from nltk.tokenize import word_tokenize
import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from nltk.classify import ClassifierI
from statistics import mode

#extra nltk downloads
nltk.download("stopwords")
nltk.download("punkt")
nltk.download("averaged_perceptron_tagger")
# In[10]:


train_file = open("./train.ft.txt", "r", encoding="utf-8")
# test_file = open("./test.ft.txt", "r", encoding="utf-8")

# In[89]:

training_set = train_file.readlines()
training_set = training_set[:50000]
print("Training list read completly")


stop_words = stopwords.words("english")

#  j is adjective, r is adverb, and v is verb
#allowed_word_types = ["J","R","V"]
allowed_word_types = ["J"]
documents = []
all_words = []

for i,line in enumerate(training_set):
    try:
        doc = re.split(r"__label__(\d+) ", str(line))[1:]
        sentiment = "nag" if doc[0] == "1" else "pos"
        review = doc[1]
        documents.append([review, sentiment])
        words = [w for w in word_tokenize(review) if w not in stop_words]
        pos = nltk.pos_tag(words)
        for w in pos:
            if w[1][0] in allowed_word_types:
                all_words.append(w[0].lower())
    except:
        print("i:{}, line:{} has some problem".format(i, line))

print("Training set opened")


# In[ ]:
save_documents = open("pickled_algos/documents.pickle","wb")
pickle.dump(documents, save_documents)
save_documents.close()


print("Documents pickled")


all_words = nltk.FreqDist(all_words)

# word_features = list(all_words.keys())[:5000] # top occuring words
word_features = list(all_words.keys()) # top occuring words
word_features = word_features[0:5000]

def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features


save_word_features = open("pickled_algos/word_features5k.pickle","wb")
pickle.dump(word_features, save_word_features)
save_word_features.close()

# creates a list of dictionary of all_words with keys and value as 
# (if particular adjective found in that particluar document)
featuresets = [(find_features(rev), category) for (rev, category) in documents]

random.shuffle(featuresets)
testing_set = featuresets[0:3000]
training_set = featuresets[3000:]

print("Training set created")

# In[102]:


classifier = nltk.NaiveBayesClassifier.train(training_set)
print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(classifier, testing_set))*100)
classifier.show_most_informative_features(15)

save_classifier = open("pickled_algos/originalnaivebayes5k.pickle","wb")
pickle.dump(classifier, save_classifier)
save_classifier.close()

MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
print("MNB_classifier accuracy percent:", (nltk.classify.accuracy(MNB_classifier, testing_set))*100)

save_classifier = open("pickled_algos/MNB_classifier5k.pickle","wb")
pickle.dump(MNB_classifier, save_classifier)
save_classifier.close()

BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
BernoulliNB_classifier.train(training_set)
print("BernoulliNB_classifier accuracy percent:", (nltk.classify.accuracy(BernoulliNB_classifier, testing_set))*100)

save_classifier = open("pickled_algos/BernoulliNB_classifier5k.pickle","wb")
pickle.dump(BernoulliNB_classifier, save_classifier)
save_classifier.close()


# In[118]:


class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)

    def confidence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)
        return conf
    
def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)
    return features


def find_sentiment(document):
    voted_classifier = VoteClassifier(classifier,
                                      MNB_classifier,
                                      BernoulliNB_classifier)
    features = find_features(document)
    return (
        voted_classifier.classify(features),
        voted_classifier.confidence(features)
    )


# failed = 0
# totalReviews = 1000

# test_set = [test_file.readline() for _ in range(10000)]
# # test_set = test_file.readlines()
# failed = 0
# totalReviews = len(test_set)


# for i,line in enumerate(test_set):
#     doc = re.split(r"__label__(\d+) ", str(line))[1:]
#     review_sent_actual = "nag" if doc[0] == "1" else "pos"
#     review = doc[1]
#     if not review_sent_actual == find_sentiment(review)[0]:
#         failed += 1

# print("Total failed: ", failed)
# print("Total passed: ", totalReviews-failed)
# print("accuracy = ", (1-failed/totalReviews)*100, "%")

# with open("./out.log", "w") as f:
#     f.write("Total failed: ", failed)
#     f.write("Total passed: ", totalReviews-failed)
#     f.write("accuracy = ", (1-failed/totalReviews)*100, "%")