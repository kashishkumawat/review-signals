import textstat
from textblob import TextBlob
from textblob import Word
import collections
from collections import Counter
import pandas as pd
import numpy as np
import nltk
import webbrowser
from jinja2 import Environment, FileSystemLoader
import os
 

reviews = pd.read_csv('B077RV8CCY.csv', encoding = "utf-8")
num_of_reviews = len(reviews.index)

readability_score = [None]*num_of_reviews
ari = [None]*num_of_reviews
read_deviation = [None]*num_of_reviews
ari_deviation = [None]*num_of_reviews
review_length = [None]*num_of_reviews
fail_reason = [None]*num_of_reviews
trust_score = [None]*num_of_reviews
toxic_score = [None]*num_of_reviews
ratings = [None]*num_of_reviews
result = [None]*num_of_reviews
review_lang = [None]*num_of_reviews
noun_phrases = []
num_nouns = [None]*num_of_reviews
num_verbs = [None]*num_of_reviews
sentiment_score = [None]*num_of_reviews
what_users_are_saying = []
# spellcheck_score = []

OldRange = (1.0 + 1.0) 
NewRange = (5.0 - 1.0)  

#####################################################
#Traverse through the Data
#####################################################

for x in range(num_of_reviews):
	test_data = (
		#Review text field in csv file
	    str(reviews.iloc[x,6])
	)
	textblob_data = TextBlob(test_data)


	#For language translation
	'''
	review_lang[x] = textblob_data.detect_language()
	if review_lang[x] != 'en':
		translated_textblob_data = textblob_data.translate(to='en')
		test_data = ( translated_textblob_data
		)	
	'''

	#Calculating Number of Verbs & Nouns
	# tokens = nltk.word_tokenize(test_data.lower())
	# text = nltk.Text(tokens)
	# tags = nltk.pos_tag(text)
	# counts = Counter(tag for word,tag in tags)

	# num_nouns[x] = counts['NN'] + counts['NNS'] + counts['NNPS']
	# num_verbs[x] = counts['VB'] + counts['VBD'] + counts['VBG'] + counts['VBN'] + counts['VBP'] + counts['VBZ']

	readability_score[x] = textstat.flesch_reading_ease(test_data)
	ari[x] = textstat.automated_readability_index(test_data)
	review_length[x] = len(str(reviews.iloc[x,6]))
	ratings[x] = reviews.iloc[x,1]

	polarity = textblob_data.sentiment.polarity
	sentiment_score[x] = (((polarity + 1.0) * NewRange) / OldRange) + 1.0

	# noun_phrases.append(textblob_data.noun_phrases)

	#For integrating Spellchecker
	'''
	for word in textblob_data.split():
		w = Word(word)
		spellcheck_score.append(w.spellcheck())
	'''	

############################################	
#Calculating the Trust Score and Toxic Score
############################################

avg_ratings = sum(ratings) / len(ratings)	
avg_review_length = sum(review_length) / len(review_length)
read_mean = sum(readability_score) / len(readability_score) 
ari_mean = sum(ari) / len(ari) 
sentiment_mean = sum(sentiment_score) / len(sentiment_score)
num_verified_purchase = 0
# avg_nouns = sum(num_nouns) / len(num_nouns)
# avg_verbs = sum(num_verbs) / len(num_verbs)

for x in range(num_of_reviews):
	read_deviation[x] = (abs(readability_score[x]-read_mean))/read_mean*100
	ari_deviation[x] = (abs(ari[x]-ari_mean))/ari_mean*100
	trust_score[x] = 0
	toxic_score[x] = 0

	if reviews.iloc[x,4] == 'Verified Purchase':
		trust_score[x] += 8
		num_verified_purchase +=1
	#Images	
	if reviews.iloc[x,9] == 1:
		trust_score[x] += 5
	if reviews.iloc[x,9] >= 2:
		trust_score[x] += 7		

	if read_deviation[x] >= 20:
		if ari_deviation[x] >= 20:
			toxic_score[x] += 3
			fail_reason[x] = "Readability Test Failed"

	if read_deviation[x] > 30:
		if ari_deviation[x] > 30:
			toxic_score[x] += 2	

	if review_length[x]	> 300 and review_length[x] < 3000:
		trust_score[x] += review_length[x]/300

	if review_length[x]	> 3000:
		trust_score[x] += 12	

	if review_length[x] < 150:
		toxic_score[x] += 2
		if review_length[x] < 100:
			toxic_score[x] += 1
			if review_length[x] < 50:
				toxic_score[x] += 1
				if review_length[x] < 10:
					toxic_score[x] += 2
		fail_reason[x] = "Review Too Short"

	if ratings[x] == 5 or ratings[x] == 0:
		toxic_score[x] += 1

	if ratings[x] >= 2 and ratings[x] <= 4:
		trust_score[x] += 1	

	if read_deviation[x] < 15:
		trust_score[x] += 1	

	if ari_deviation[x] < 15:	
		trust_score[x] += 1	

	if abs(sentiment_score[x]-ratings[x]) > 1.8:
		toxic_score[x] += 1

	if toxic_score[x] >= trust_score[x]:
		result[x] = 0
	else:
		result[x] = 1	

trust_mean = sum(trust_score) / len(trust_score) 
toxic_mean = sum(toxic_score) / len(toxic_score) 

######################################################
#Calculating New Ratings after Discarding Fake Reviews
######################################################

fail_count = 0
new_total_ratings = 0
new_sentiment_total = 0
for x in range(num_of_reviews):
	if result[x] == 0:
		fail_count += 1
	else:	
		new_total_ratings += ratings[x]
		new_sentiment_total += sentiment_score[x]

passed_count = num_of_reviews - fail_count
new_ratings = new_total_ratings/passed_count
new_sentiment_score = new_sentiment_total/passed_count
percent_failed = fail_count/num_of_reviews*100
num_non_verified_purchase = num_of_reviews - num_verified_purchase 

#Counting Number of Reviews for each ratings
num_five_stars = 0
num_four_stars = 0
num_three_stars = 0
num_two_stars = 0
num_one_stars = 0
num_failed_five_stars = 0
num_failed_four_stars = 0
num_failed_three_stars = 0
num_failed_two_stars = 0
num_failed_one_stars = 0


for x in range(num_of_reviews):
	if ratings[x] == 5:
		num_five_stars +=1
		if result[x] == 0:
			num_failed_five_stars +=1
	if ratings[x] == 4:	
		num_four_stars +=1
		if result[x] == 0:
			num_failed_four_stars +=1
	if ratings[x] == 3:	
		num_three_stars +=1
		if result[x] == 0:
			num_failed_three_stars +=1
	if ratings[x] == 2:	
		num_two_stars +=1
		if result[x] == 0:
			num_failed_two_stars +=1
	if ratings[x] == 1:	
		num_one_stars +=1	
		if result[x] == 0:
			num_failed_one_stars +=1	

#########################################################
#Printing Output in Terminal
#########################################################
print('\nMean Automated Readability Index = ', ari_mean)
print('\nMean Flesch-Kincaid Readability = ', read_mean)
print('\nAverage Ratings = ', round(avg_ratings, 2))
print('\nAverage Sentiment Polarity = ', sentiment_mean)
print('\nTotal Number of Reviews = ', num_of_reviews)
print('\nNumber of 5,4,3,2,1 Star Reviews = ', num_five_stars, num_four_stars, num_three_stars, num_two_stars, num_one_stars)
print('\nNumber of Failed Reviews = ', fail_count)
print('\nNumber of Failed 5,4,3,2,1 Star Reviews = ', num_failed_five_stars, num_failed_four_stars, num_failed_three_stars, num_failed_two_stars, num_failed_one_stars)
print('\nRatings After Discarding Fake Reviews = ', new_ratings)
print('\nSentiment After Discarding Fake Reviews = ', new_sentiment_score)
print('\nMost Common Phrases = ')

noun_phrases_list = sum(noun_phrases, [])
counter = collections.Counter(noun_phrases_list)
most_common_words = counter.most_common()

for i in range(len(most_common_words)):
	if most_common_words[i][1] > 2:
		if len(most_common_words[i][0].split()) > 2:
			print(most_common_words[i][0])
			what_users_are_saying.append(most_common_words[i][0])

##############################################################
#Writing Data to CSV File
##############################################################

reviews['Flesch-Kincaid Readability'] = readability_score
reviews['ARI'] = ari
reviews['Readibility Deviation'] = read_deviation
reviews['ARI Deviation'] = ari_deviation
reviews['Length'] = review_length
reviews['Trust Score'] = trust_score
reviews['Toxic Score'] = toxic_score
reviews['Sentiment Score'] = sentiment_score
reviews['Number of Nouns'] = num_nouns
reviews['Number of Verbs'] = num_verbs
reviews['Passed or Not'] = result
reviews['Issues'] = fail_reason
reviews.to_csv('output.csv')

##############################################################
#Writing Data to HTML File
##############################################################


root = os.path.dirname(os.path.abspath(__file__))
templates_dir = os.path.join(root, 'templates')
env = Environment( loader = FileSystemLoader(templates_dir) )
template = env.get_template('index.html')

 
filename = os.path.join(root, 'html', 'index.html')
with open(filename, 'w') as fh:
    fh.write(template.render(
        h1 = "Realme U1",
        amazon_ratings = round(avg_ratings, 1),
        adjusted_ratings = round(new_ratings, 1),
        fake_reviews = round(percent_failed, 1),
        names    = ["Foo", "Bar", "Qux"],
        num_five_stars = num_five_stars,
		num_four_stars = num_four_stars,
		num_three_stars = num_three_stars,
		num_two_stars = num_two_stars,
		num_one_stars = num_one_stars,
		num_failed_five_stars = num_failed_five_stars,
		num_failed_four_stars = num_failed_four_stars,
		num_failed_three_stars = num_failed_three_stars,
		num_failed_two_stars = num_failed_two_stars,
		num_failed_one_stars = num_failed_one_stars,
		total_reviews = num_of_reviews, 
		fail_count = fail_count,
		sentiment_mean = round(sentiment_mean, 2), 
		new_sentiment_score = round(new_sentiment_score, 2),
		read_mean = round(read_mean, 2),
		ari_mean = round(ari_mean, 2),
		avg_review_length = round(avg_review_length),
		num_non_verified_purchase = num_non_verified_purchase,
		avg_nouns = round(avg_nouns),
		avg_verbs = round(avg_verbs),
		what_users_are_saying1 = what_users_are_saying[0],
		what_users_are_saying2 = what_users_are_saying[1],
		what_users_are_saying3 = what_users_are_saying[2],
    ))

url = "C:/Users/Kashish/Desktop/sentiment-analysis/review_analysis/html/index.html"
webbrowser.open(url, new=2) 
