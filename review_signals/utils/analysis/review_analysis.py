import collections
import logging
import os
import re
from collections import Counter

import nltk
import numpy as np
import pandas as pd
import textstat
from tqdm import tqdm
from pymongo import MongoClient
from textblob import TextBlob, Word
from review_signals.core.models import (
    ProductCategory, 
    Product,
    ProductReport,
)
import os
logger = logging.getLogger(__name__)


class Report:
    def __init__(self, db_name = "review_signal", collection_name= "amazonData"):
        self.database = db_name
        self.collection = collection_name
        self.setup_mongo()

    def setup_mongo(self):
        self.mongo_client = MongoClient()
        if self.database not in self.mongo_client.list_database_names():
            print("Database does not exists. Try again later !!")
            os._exit(1)

        if self.collection not in self.mongo_client[self.database].collection_names():
            print("Collection does not exists. Try again later !!")
            os._exit(1)

        self.database = self.mongo_client.get_database(self.database)
        self.collection = self.database.get_collection(self.collection)

    def execute(self):
        """entry-point"""
        self.yield_keywords()

    def yield_keywords(self):
        for row in tqdm(
            self.collection.find(), total=self.collection.estimated_document_count()
        ):
            keyword = row.get("keyword")
            products = row.get("products")
            instance, created = ProductCategory.objects.get_or_create(
                name=" ".join((word.capitalize() for word in keyword.split("-"))),
                slug=keyword,
            )
            self.analyze_products(products, instance)
            self.rank_products(instance)

    def rank_products(self, product_category):
        reports = ProductReport.objects.filter(product__category=product_category).order_by('-rs_average_rating')
        for rank, report in enumerate(reports):
            product = report.product
            product.rank = rank + 1
            product.score = round(2 * report.rs_average_rating, 2)
            product.save()

    def analyze_products(self, products, product_category):
        for product in tqdm(products):
            reviews = product.pop("reviews")
            product_instance = self.create_product(product, product_category)
            self.analyze_reviews(product_instance, product, reviews)

    @staticmethod
    def create_product(product, product_category):
        product_instance, created = Product.objects.get_or_create(
            name=product["name"],
            url=product["url"],
            asin=product["asin"],
            price=product["price"],
            image=product['img'],
        )
        product_instance.category = product_category
        product_instance.save()
        return product_instance

    def analyze_reviews(self, product_instance, product, reviews):
        if not reviews:
            return

        self.reviews_data = dict.fromkeys(
            [
                "readability_score",
                "ari",
                "is_fake_review",
                "sentiment_score",
            ],
            [None] * len(reviews),
        )

        self.reviews_data["readability_level_count"] = dict.fromkeys(range(1, 8), 0)
        self.reviews_data["ari_score_count"] = dict.fromkeys(range(1, 15), 0)
        avg_nouns, avg_verbs, noun_phrases = self.calculate_verbs_nouns_average(reviews)
        mean_readability_score = self.calculate_mean_readability_score(reviews)
        ari_mean = self.calculate_mean_ari(reviews)
        avg_review_length = self.calculate_average_review_length(reviews)
        avg_ratings = self.calculate_average_ratings(reviews)
        sentiment_mean = self.calculate_mean_sentiment_score(reviews)
        fake_reviews_count = self.detect_fake_reviews(reviews, ari_mean, mean_readability_score)
        new_avg_ratings = self.calculate_average_ratings(reviews, exclude_fake=True)
        new_sentiment_mean = self.calculate_mean_sentiment_score(
            reviews, exclude_fake=True
        )
        ratings_counter, fake_reviews_rating_counter = self.calculate_rating_review_count(
            reviews
        )
        extra_data = {
            'noun_phrases': noun_phrases,
            'most_common_phrases': self.get_most_common_phrases(noun_phrases),
        }
        try:
            total_reviews = int(re.sub(",", "", product["total_reviews"]))
        except:
            total_reviews = 0
        
        ProductReport.objects.get_or_create(
            product=product_instance,
            average_readability_score=round(mean_readability_score, 2),
            average_ari=round(ari_mean, 2),
            average_noun_count=round(avg_nouns, 2),
            average_verb_count=round(avg_verbs, 2),
            average_review_length=round(avg_review_length),
            average_sentiment_score=round(sentiment_mean, 2),
            average_rating=round(avg_ratings, 2),
            fake_reviews_count=fake_reviews_count,
            rs_average_rating=round(new_avg_ratings, 2),
            rs_average_sentiment_score=round(new_sentiment_mean, 2),
            rating_counter=ratings_counter,
            fake_reviews_rating_counter=fake_reviews_rating_counter,
            readability_level_count=self.reviews_data.get('readability_level_count'),
            ari_score_count=self.reviews_data.get('ari_score_count'),
            total_reviews=sum(ratings_counter.values()),
            extra_data=extra_data
        )

    @staticmethod
    def calculate_verbs_nouns_average(reviews):
        noun_count = 0
        verb_count = 0
        noun_phrases = []

        for review in reviews:
            review_text = review.get("review_text")
            textblob_data = TextBlob(review_text)
            tokens = nltk.word_tokenize(review_text.lower())
            text = nltk.Text(tokens)
            tags = nltk.pos_tag(text)
            counts = Counter(tag for word, tag in tags)
            noun_count += counts["NN"] + counts["NNS"] + counts["NNPS"]
            verb_count += (
                counts["VB"]
                + counts["VBD"]
                + counts["VBG"]
                + counts["VBN"]
                + counts["VBP"]
                + counts["VBZ"]
            )
            noun_phrases.append(textblob_data.noun_phrases)

        avg_nouns = noun_count / len(reviews)
        avg_verbs = verb_count / len(reviews)
        return (avg_nouns, avg_verbs, noun_phrases)

    def calculate_mean_readability_score(self, reviews):
        readability_score_sum = 0
        readability_level_count =self.reviews_data.get('readability_level_count')
        readability_score_list = self.reviews_data.get("readability_score")

        for index, review in enumerate(reviews):
            review_text = review.get("review_text")
            readability_score = textstat.flesch_reading_ease(review_text)
            readability_score_sum += readability_score
            readability_score_list[index] = readability_score

            if readability_score <= 100 and readability_score > 90:
                readability_level_count[1] += 1
            elif readability_score > 80:
                readability_level_count[2] += 1
            elif readability_score > 70:
                readability_level_count[3] += 1
            elif readability_score > 60:
                readability_level_count[4] += 1
            elif readability_score > 50:
                readability_level_count[5] += 1
            elif readability_score > 30:
                readability_level_count[6] += 1
            elif readability_score > 0:
                readability_level_count[7] += 1
        self.reviews_data['readability_level_count'] = readability_level_count
        return readability_score / len(reviews)  # mean

    def calculate_mean_ari(self, reviews):
        ari_sum = 0
        ari_list = self.reviews_data.get("readability_score")
        ari_score_count = self.reviews_data.get("ari_score_count")
        for index, review in enumerate(reviews):
            review_text = review.get("review_text")
            review_ari = textstat.automated_readability_index(review_text)
            ari_list[index] = review_ari
            ari_sum += review_ari
            if round(review_ari) in range(1, 15):
                ari_score_count[round(review_ari)] += 1
        self.reviews_data["ari_score_count"] = ari_score_count
        return ari_sum / len(reviews)  # mean

    @staticmethod
    def calculate_average_review_length(reviews):
        total_review_length = 0
        for review in reviews:
            review_text = review.get("review_text")
            total_review_length += len(review_text)
        avg_review_length = total_review_length / len(reviews)
        return avg_review_length

    def calculate_average_ratings(self, reviews, exclude_fake=False):
        from random import uniform
        review_ratings_sum = sum(int(review.get("stars")) for review in reviews)
        average_rating = review_ratings_sum / len(reviews)
        if exclude_fake:
            is_fake_review_list = self.reviews_data.get("is_fake_review")
            new_review_ratings_sum = sum(
                int(review.get("stars"))
                for index, review in enumerate(reviews)
                if is_fake_review_list[index]
            )
            new_average_rating = new_review_ratings_sum / len(reviews)
            if abs(average_rating - new_average_rating) > 1.5:
                if new_average_rating > average_rating:
                    average_rating += round(uniform(0.5, 1), 2)
                else:
                    average_rating -= round(uniform(0.5, 1), 2)
        return average_rating

    @staticmethod
    def get_polarity(review):
        return TextBlob(review.get("review_text")).sentiment.polarity

    def get_sentiment_score(self, review):
        new_range, old_range = (1.0 + 1.0), (5.0 - 1.0)
        return (((self.get_polarity(review) + 1.0) * new_range) / old_range) + 1.0

    def calculate_mean_sentiment_score(self, reviews, exclude_fake=False):
        if exclude_fake:
            is_fake_review_list = self.reviews_data.get("is_fake_review")
            sentiment_score_sum = sum(
                self.get_sentiment_score(review)
                for index, review in enumerate(reviews)
                if is_fake_review_list[index]
            )
        else:
            sentiment_score_sum = sum(
                self.get_sentiment_score(review) for review in reviews
            )
        return sentiment_score_sum / len(reviews)

    def detect_fake_reviews(self, reviews, ari_mean, read_mean):
        fake_reviews_count = 0
        num_verified_purchase = 0
        is_fake_review_list = self.reviews_data.get("is_fake_review")
        readability_score = self.reviews_data.get("readability_score")
        sentiment_score = self.reviews_data.get("sentiment_score")
        ari = self.reviews_data.get("ari")

        for index, review in enumerate(reviews):
            ari_deviation = (abs(ari[index] - ari_mean)) / ari_mean * 100
            read_deviation = (
                (abs(readability_score[index] - read_mean)) / read_mean * 100
            )
            trust_score = 0
            toxic_score = 0

            if review.get("badges") == "Verified Purchase":
                trust_score += 8
                num_verified_purchase += 1

            if review.get("image_count") == 1:
                trust_score += 5
            if review.get("image_count") >= 2:
                trust_score += 7

            if read_deviation >= 20:
                if ari_deviation >= 20:
                    toxic_score += 3

            if read_deviation > 30:
                if ari_deviation > 30:
                    toxic_score += 2

            review_length = len(review.get("review_text"))
            if review_length > 300 and review_length < 3000:
                trust_score += review_length / 300

            if review_length > 3000:
                trust_score += 12

            if review_length < 150:
                toxic_score += 2
                if review_length < 100:
                    toxic_score += 1
                    if review_length < 50:
                        toxic_score += 1
                        if review_length < 10:
                            toxic_score += 2

            ratings = review.get("stars")

            if ratings in (0, 5):
                toxic_score += 1

            if ratings in range(2, 5):
                trust_score += 1

            if read_deviation < 15:
                trust_score += 1

            if ari_deviation < 15:
                trust_score += 1

            if abs(sentiment_score[index] - ratings) > 1.8:
                toxic_score += 1

            if toxic_score >= trust_score:
                is_fake_review_list[index] = True
                fake_reviews_count += 1
            else:
                is_fake_review_list[index] = False

        return fake_reviews_count

    def calculate_rating_review_count(self, reviews):
        is_fake = self.reviews_data.get("is_fake_review")
        ratings_counter = Counter((review.get("stars") for review in reviews))
        fake_reviews_rating_counter = Counter(
            (
                review.get("stars")
                for index, review in enumerate(reviews)
                if is_fake[index]
            )
        )
        return ratings_counter, fake_reviews_rating_counter


    @staticmethod
    def get_most_common_phrases(noun_phrases):
        most_common_phrases = []
        noun_phrases_list = sum(noun_phrases, [])
        counter = collections.Counter(noun_phrases_list)
        most_common_words = counter.most_common()

        for i in range(len(most_common_words)):
            if most_common_words[i][1] > 2:
                if len(most_common_words[i][0].split()) > 2:
                    most_common_phrases.append(most_common_words[i][0])

        return most_common_words
